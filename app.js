const express = require("express");
const app = express();

const autocompleteRoutes = require("./api/routes/autocompleteSuggestion");
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );

  next();
});
app.use("/autocomplete", autocompleteRoutes);

module.exports = app;
