const express = require("express");
const router = express.Router();
const axios = require("axios");

router.get("/", async (req, res, next) => {
  const autoCompleteURL = new URL(
    "https://maps.googleapis.com/maps/api/place/autocomplete/json"
  );
  const APIKey = "AIzaSyDBZIXSclXCvGim_n17QvvjVhyxlutoqKA";
  const input = `${req.query.barangay} ${req.query.municipality} ${req.query.province} Philippines`;
  autoCompleteURL.search = new URLSearchParams({
    input: input,
    types: "geocode",
    key: APIKey,
  });
  const getPlaceId = await axios.get(autoCompleteURL.toString());

  if (getPlaceId.data.predictions.length === 0) {
    return res.status(200).json({
      message: "No location matched with the given data.",
    });
  }

  const matchedPlace = getPlaceId.data.predictions[0];
  const placeId = matchedPlace.place_id;
  const placeDetailsURL = new URL(
    "https://maps.googleapis.com/maps/api/place/details/json"
  );
  placeDetailsURL.search = new URLSearchParams({
    fields: "formatted_address,geometry",
    place_id: placeId,
    key: APIKey,
  });
  const getPlaceDetails = await axios.get(placeDetailsURL.toString());
  const placeDetailsResult = getPlaceDetails.data.result;
  if (placeDetailsResult.length === 0) {
    return res.status(200).json({
      message: `Cannot find longitude and latitude of ${req.query.barangay}, ${req.query.municipality}, ${req.query.province}.`,
    });
  }

  console.log(placeDetailsResult.formatted_address);
  const latitude = placeDetailsResult.geometry.location.lat;
  const longitude = placeDetailsResult.geometry.location.lng;
  autoCompleteURL.search = new URLSearchParams({
    input: req.query.searchText,
    location: `${latitude},${longitude}`,
    radius: 10000,
    key: APIKey,
  });

  console.log(autoCompleteURL.toString());
  const getInputPlace = await axios.get(autoCompleteURL.toString());
  const inputPlacePredictions = getInputPlace.data.predictions;
  if (inputPlacePredictions.length === 0) {
    return res.status(200).json({
      message: "No location matched with the given data.",
    });
  }

  // Output only one address
  // const firstPrediction = inputPlacePredictions[0];
  // const predictionTerms = firstPrediction.terms;
  // if (predictionTerms.length > 5) {
  //   return res.status(200).json([
  //     {
  //       address: firstPrediction.description,
  //       barangay: predictionTerms[1].value,
  //       municipality: predictionTerms[3].value,
  //       province: predictionTerms[4].value,
  //     },
  //   ]);
  // }

  // return res.status(200).json([
  //   {
  //     address: firstPrediction.description,
  //     barangay: predictionTerms[1].value,
  //     municipality: predictionTerms[2].value,
  //     province: predictionTerms[3].value,
  //   },
  // ]);

  // Output multiple addresses
  const responseArray = inputPlacePredictions.map((prediction) => {
    if (prediction.terms) {
      if (prediction.terms.length > 5) {
        return {
          address: prediction.description,
          barangay: prediction.terms[1].value,
          municipality: prediction.terms[3].value,
          province: prediction.terms[4].value,
        };
      } else if (prediction.terms.length > 3 && prediction.terms.length <= 5) {
        return {
          address: prediction.description,
          barangay: prediction.terms[1].value,
          municipality: prediction.terms[2].value,
          province: prediction.terms[3].value,
        };
      } else {
        return {
          address: prediction.description,
        };
      }
    }
  });

  res.header(`Access-Control-Allow-Origin`, `*`);
  res.header(`Access-Control-Allow-Methods`, `GET,PUT,POST,DELETE`);
  res.header(`Access-Control-Allow-Headers`, `Content-Type`);
  return res.status(200).json(responseArray);
});

module.exports = router;
